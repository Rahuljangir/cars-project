module.exports = function(inventory)
{
    let result;
    if(!inventory)
    {
        return [];
    }
   
    if(Array.isArray(inventory))
    {
        let car;
        car = inventory[inventory.length-1];
        
        result = "last car is a "+car.car_make+" "+car.car_model;
        return result;
    }
   
}
